#abhishek Containers

abhishek container package lets you add a beautiful property container to your Flutter app.

## Installation

1. Add the latest version of package to your pubspec.yaml (and run`dart pub get`):
```yaml
dependencies:
  abhishek_container1: ^0.0.1
```
2. Import the package and use it in your Flutter App.
```dart
import 'package:abhishek_container1/abhishek_container1.dart';
```
<hr>

<table>
<tr>
<td>

```dart
class abhishek_container extends StatefulWidget {
  const abhishek_container({Key? key}) : super(key: key);

  @override
  _abhishek_containerState createState() => _abhishek_containerState();
}

class _abhishek_containerState extends State<abhishek_container> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

```

</td>
</tr>
</table>

